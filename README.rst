``cesar``
=========

not `this one`_, `that one`_.

.. _`this one`: https://en.wikipedia.org/wiki/Julius_Caesar
.. _`that one`: https://en.wikipedia.org/wiki/César_Baldaccini

|Source| |PyPI| |Travis| |Codecov| |Codacy| |Format| |License| |SayThanks|

.. |Source| image:: https://img.shields.io/badge/source-GitHub-303030.svg?maxAge=3600&style=flat-square
   :target: https://github.com/althonos/cesar

.. |Codacy| image:: https://img.shields.io/codacy/grade/a3948501f511499fbf23f6f4e29c7a6c/master.svg?style=flat-square&maxAge=300
   :target: https://www.codacy.com/app/althonos/cesar/dashboard

.. |Travis| image:: https://img.shields.io/travis/althonos/cesar/master.svg?style=flat-square&maxAge=300
   :target: https://travis-ci.org/althonos/cesar/branches

.. |Codecov| image:: https://img.shields.io/codecov/c/github/althonos/cesar/master.svg?style=flat-square&maxAge=300
   :target: https://codecov.io/gh/althonos/cesar

.. |PyPI| image:: https://img.shields.io/pypi/v/cesar.svg?style=flat-square&maxAge=300
   :target: https://pypi.python.org/pypi/cesar

.. |Format| image:: https://img.shields.io/pypi/format/cesar.svg?style=flat-square&maxAge=300
   :target: https://pypi.python.org/pypi/cesar

.. |Versions| image:: https://img.shields.io/pypi/pyversions/cesar.svg?style=flat-square&maxAge=300
   :target: https://travis-ci.org/althonos/cesar

.. |License| image:: https://img.shields.io/pypi/l/cesar.svg?style=flat-square&maxAge=300
   :target: https://choosealicense.com/licenses/mit/

.. |SayThanks| image:: https://img.shields.io/badge/say-thanks!-1EAEDB.svg?maxAge=86400&style=flat-square
   :target: https://saythanks.io/to/althonos


Overview
''''''''

``cesar`` is a library that provides Python programmers a single API to use
several algorithms of compression. It aims to become to compression libraries
what `fs <https://github.com/Pyfilesystem/pyfilesystem2>`_ is to filesystems,
or `libcloud <https://libcloud.apache.org/>`_ to cloud service providers.
The public API itself is inspired by the interface of the
`bz2 <https://docs.python.org/3.6/library/bz2.html?highlight=bz2#module-bz2>`_
module of the Python standard library.


Examples
''''''''

First of all import the module: all the useful functions do not require an
additional import step.

.. code:: python

  >>> import cesar


Compressed File
---------------

Want to open a compressed file based and guess the compression algorithm used
from the file extension ? No problem, ``cesar.file.open`` will return an object
that implements the `Python file interface <https://docs.python.org/3.6/library/io.html#io.RawIOBase>`_:

.. code:: python

  >>> with cesar.file.open('myfile.txt.bz2', 'wb') as f:
  ...     f.write(b'I will be compressed on the fly !')
  0
  >>> with cesar.file.open('myfile.txt.bz2', 'rb') as g:
  ...     g.read()
  b'I will be compressed on the fly !'
  >>> with open('myfile.txt.bz2', 'rb') as h:
  ...     h.read(15)
  b'BZh11AY&SY\xb2\xc5\xa0\t\x00'


Compressed data
---------------

Want to compress some data quickly in a single call ? There is a single
function for that:

.. code:: python

  >>> cesar.core.compress('gz', b'Hi!')
  b'\x1f\x8b\x08\x00\x00\x00\x00\x00\x04\x03\xf3\xc8T\x04\x00\xda\xc5\x9ey\x03\x00\x00\x00'

Then, decompress it in one go:

.. code:: python

  >>> cesar.core.decompress('gz', _)
  b'Hi!'


Compressed buffered stream
--------------------------

Maybe you need to compress data that comes in several blocks.
For this, use the ``Compressor`` interface:

1. First of all, get a compressor for the desired compression format,
   using the ``cesar.get_compressor`` function:

   .. code:: python

     >>> compressor = cesar.core.get_compressor('lzma')
     >>> type(compressor)
     <class 'cesar.codecs.lzma.LzmaCompressor'>

2. Let us use a ``bytearray`` as our destination buffer, although we could
   also use a file, or an ordered collection:

   .. code:: python

     >>> buffer = bytearray()

3. Compress all of the blocks one after the other, putting the returned
   binary string into the output buffer:

   .. code:: python

     >>> for block in (b'This ', b'data ', b'comes ', b'in ', b'chunks'):
     ...     buffer.extend(compressor.compress(block))

4. Finally, finish the compressor to write the terminating data chunk
   (such as a checksum, a special signature, etc.). The compressor
   cannot be used after this call, and the buffer cannot be concatenated
   with more data:

   .. code:: python

     >>> buffer.extend(compressor.finish())

   Not calling ``compressor.finish`` will raise a ``ResourceWarning`` when the
   compressor is discarded.

5. A quick check will show that our data was compressed as intended:

   .. code:: python

     >>> cesar.core.decompress('lzma', buffer)
     b'This data comes in chunks'


Installation
''''''''''''

Basic installation
------------------

Install with pip to get the latest stable version::

  pip install --user cesar

``cesar`` itself only depends on `six <https://github.com/benjaminp/six>`_ to
help writing version-agnostic code.


Additional compression support
------------------------------

Some compression algorithms have additional dependencies. To make sure `cesar`
support them, install the desired extras as well from the list of available
extras below.

==========   =====================================   ============================
extra        description                             requirements
==========   =====================================   ============================
**brotli**   New algorithm developed by Google       |brotli|
**lzma**     Lempel-Ziv-Markov chain algorithm       |backports.lzma| *(for Py2)*
==========   =====================================   ============================

.. |brotli| image:: https://img.shields.io/badge/pypi-brotli-blue.svg?style=flat-square&maxAge=3600
   :target: https://pypi.python.org/pypi/brotli

.. |backports.lzma| image:: https://img.shields.io/badge/pypi-backports.lzma-blue.svg?style=flat-square&maxAge=3600
   :target: https://pypi.python.org/pypi/backports.lzma


For instance, to make sure **brotli** and **lzma** are supported, install
``cesar`` with both of these extras::

  pip install --user cesar[brotli,lzma]
