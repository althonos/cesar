# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from cesar.test import CesarTestCase
from cesar.codecs.brotli import BrotliCompressor, BrotliDecompressor


class TestBrotli(CesarTestCase, unittest.TestCase):
    _compressor = BrotliCompressor
    _decompressor = BrotliDecompressor
