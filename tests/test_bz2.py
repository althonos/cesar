# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from cesar.test import CesarTestCase
from cesar.codecs.bz2 import Bz2Compressor, Bz2Decompressor


class TestBz2(CesarTestCase, unittest.TestCase):
    _compressor = Bz2Compressor
    _decompressor = Bz2Decompressor
