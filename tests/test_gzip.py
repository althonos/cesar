# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from cesar.test import CesarTestCase
from cesar.codecs.gzip import GzipCompressor, GzipDecompressor


class TestGzip(CesarTestCase, unittest.TestCase):
    _compressor = GzipCompressor
    _decompressor = GzipDecompressor
