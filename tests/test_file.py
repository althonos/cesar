# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import io
import os
import shutil
import tempfile
import unittest

import cesar
from cesar.file import CompressedFileReader, CompressedFileWriter
from cesar.base import Compressor

from .utils import on_all_compressions


@on_all_compressions
class TestFile(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def _test(self, compression):

        compressor = cesar.core.get_compressor(compression)
        decompressor = cesar.core.get_decompressor(compression)
        filename = os.path.join(self.tmpdir, 'test.{}'.format(compression))

        with CompressedFileWriter(io.open(filename, 'wb'), compressor) as f:
            f.writelines([b"abc\n", b"def\n", b"ghi\n"])
            x = f.write(b'text\n')

        self.assertTrue(f.closed)
        self.assertLessEqual(x, len(b'text\n'))
        self.assertTrue(os.path.isfile(filename))

        with CompressedFileReader(io.open(filename, 'rb'), decompressor) as f:
            l1 = f.readline()
            l2 = next(iter(f))
            rest = f.read()

        self.assertTrue(f.closed)
        self.assertEqual(l1, b'abc\n')
        self.assertEqual(l2, b'def\n')
        self.assertEqual(rest, b'ghi\ntext\n')

        decompressor.reset()

        with CompressedFileReader(io.open(filename, 'rb'), decompressor) as f:
            text = f.read(7)
            rest = f.read(100)

        self.assertEqual(text, b'abc\ndef')
        self.assertEqual(rest, b'\nghi\ntext\n')

        filename = filename.encode('utf-8')
        decompressor.reset()

        with CompressedFileReader(io.open(filename, 'rb'), decompressor) as f:
            l = list(f)

        self.assertEqual(l, [b'abc\n', b'def\n', b'ghi\n', b'text\n'])


@on_all_compressions
class TestSeek(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def _test(self, compression):

        compressor = cesar.core.get_compressor(compression)
        decompressor = cesar.core.get_decompressor(compression)
        filename = os.path.join(self.tmpdir, 'test.{}'.format(compression))

        with CompressedFileWriter(io.open(filename, 'wb'), compressor) as f:
            #self.assertFalse(f.seekable())
            f.write(b'abcdefghi')

        with CompressedFileReader(io.open(filename, 'rb'), decompressor) as f:
            self.assertTrue(f.seekable())

            self.assertEqual(f.read(), b'abcdefghi')
            self.assertEqual(f.seek(0), 0)
            self.assertEqual(f.read(), b'abcdefghi')
            self.assertEqual(f.seek(2), 2)
            self.assertEqual(f.read(), b'cdefghi')
            self.assertEqual(f.seek(0), 0)
            self.assertEqual(f.seek(2), 2)

            self.assertEqual(f.seek(0), 0)
            self.assertEqual(f.seek(0, os.SEEK_CUR), 0)
            self.assertEqual(f.seek(2, os.SEEK_CUR), 2)
            self.assertEqual(f.seek(2, os.SEEK_CUR), 4)
            self.assertEqual(f.read(), b'efghi')
            self.assertEqual(f.seek(4), 4)
            self.assertEqual(f.seek(-2, os.SEEK_CUR), 2)
            self.assertEqual(f.read(), b'cdefghi')

            self.assertEqual(f.seek(0, os.SEEK_END), 9)
            self.assertEqual(f.read(), b'')
            self.assertEqual(f.seek(-2, os.SEEK_END), 7)
            self.assertEqual(f.read(), b'hi')

            self.assertRaises(ValueError, f.seek, -2, os.SEEK_SET)
            self.assertRaises(ValueError, f.seek, +2, os.SEEK_END)
            self.assertRaises(ValueError, f.seek,  0, 5)


@on_all_compressions
class TestOpen(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def _test(self, extension):
        filename = os.path.join(self.tmpdir, "test.{}".format(extension))
        with cesar.open(filename, 'wb') as f:
            f.write(b'Hello, World !\n')
        self.assertTrue(os.path.isfile(filename))
        with cesar.open(filename, 'rb') as f:
            self.assertEqual(f.read(), b'Hello, World !\n')
