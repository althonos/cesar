# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import unittest

import cesar

from .utils import on_all_compressions, mock


class TestGetters(unittest.TestCase):

    def test_not_found(self):
        self.assertRaises(RuntimeError, cesar.core.get_compressor, 'unknown')
        self.assertRaises(RuntimeError, cesar.core.get_decompressor, 'unknown')

    def test_wrong_type(self):
        self.assertRaises(TypeError, cesar.core.get_compressor, 1)
        self.assertRaises(TypeError, cesar.core.get_decompressor, 1)

    def test_wrong_factory_type(self):
        bad_compressor = mock.MagicMock()
        bad_compressor.load = mock.MagicMock()
        with mock.patch.object(cesar.core.pkg_resources, 'iter_entry_points',
                        new_callable=mock.MagicMock()) as it:
            self.assertRaises(TypeError, cesar.core.get_compressor, 'gz')
        with mock.patch.object(cesar.core.pkg_resources, 'iter_entry_points',
                        new_callable=mock.MagicMock()) as it:
            self.assertRaises(TypeError, cesar.core.get_decompressor, 'gz')

    def test_load_failed(self):
        bad_compressor = mock.MagicMock()
        bad_compressor.load = mock.MagicMock(side_effect=ZeroDivisionError)
        with mock.patch.object(cesar.core.pkg_resources, 'iter_entry_points',
                        new_callable=mock.MagicMock()) as it:
            it.return_value = iter([bad_compressor])
            self.assertRaises(RuntimeError, cesar.core.get_compressor, 'gz')
        with mock.patch.object(cesar.core.pkg_resources, 'iter_entry_points',
                        new_callable=mock.MagicMock()) as it:
            it.return_value = iter([bad_compressor])
            self.assertRaises(RuntimeError, cesar.core.get_decompressor, 'gz')



@on_all_compressions
class TestCompress(unittest.TestCase):

    @staticmethod
    def resource(name):
        return os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'resources',
            name
        )

    def _test(self, compression):
        for i in range(1, 4):
            with open(self.resource('test{}.txt'.format(i)), 'rb') as f:
                data_exact = f.read()
            data_test = cesar.core.decompress(
                compression,
                cesar.core.compress(compression, data_exact)
            )
            self.assertEqual(data_exact, data_test)


@on_all_compressions
class TestDecompress(unittest.TestCase):

    @staticmethod
    def resource(name):
        return os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'resources',
            name
        )

    def _test(self, compression):

        for i in range(1, 4):
            path_raw = self.resource('test{}.txt'.format(i))
            path_compressed = os.extsep.join([path_raw, compression])
            with open(path_raw, 'rb') as f:
                data_exact = f.read()
            with open(path_compressed, 'rb') as g:
                data_test = cesar.core.decompress(compression, g.read())
            self.assertEqual(data_exact, data_test)
