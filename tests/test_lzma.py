# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from cesar.test import CesarTestCase
from cesar.codecs.lzma import LzmaCompressor, LzmaDecompressor


class TestLzma(CesarTestCase, unittest.TestCase):
    _compressor = LzmaCompressor
    _decompressor = LzmaDecompressor
