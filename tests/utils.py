# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest
import functools
import pkg_resources

try:
    from unittest import mock
except ImportError:
    import mock


def on_all_compressions(testcase):

    compressions = set(
        entry_point.name
            for entry_point in
                pkg_resources.iter_entry_points('cesar.compressors')
    )

    class ArchiveTestWrapper(testcase):

        @classmethod
        def register_test(cls, compression):
            def test(self):
                self._test(compression)
            test.__name__ = str("test_{}").format(compression)
            setattr(cls, test.__name__, test)

    for c in compressions:
        ArchiveTestWrapper.register_test(c)

    ArchiveTestWrapper.__module__ = testcase.__module__
    ArchiveTestWrapper.__name__ = testcase.__name__
    try:
        ArchiveTestWrapper.__qualname__ = testcase.__qualname__
    except AttributeError:
        ArchiveTestWrapper.__qualname__ = testcase.__name__

    return ArchiveTestWrapper
