# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import brotli

from .. import base


class BrotliCompressor(base.Compressor):

    def __init__(self, **options):
        super(BrotliCompressor, self).__init__()
        self._quality = options.get('quality', 11)
        self._lgwin = options.get('lgwin', 22)
        self._lgblock = options.get('lgblock', 0)
        self.reset()

    def compress(self, data):  # noqa: D102
        self._flushed = False
        return self._raw.process(data)

    def flush(self):  # noqa: D102
        if not self.flushed():
            self._flushed = True
            return self._raw.flush()
        return b''

    def finish(self):  # noqa: D102
        if not self.finished():
            self._flushed = self._finished = True
            return self._raw.finish()
        return b''

    def reset(self):  # noqa: D102
        self._raw = brotli.Compressor(
            mode=brotli.MODE_GENERIC,
            quality=self._quality,
            lgwin=self._lgwin,
            lgblock=self._lgblock,
        )


class BrotliDecompressor(base.Decompressor):

    def __init__(self, **options):
        super(BrotliDecompressor, self).__init__()

    def decompress(self, data):  # noqa: D102
        self._flushed = False
        return brotli.decompress(data) if data else b''
