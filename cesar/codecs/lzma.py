# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import sys

try:
    import lzma
except ImportError:
    from backports import lzma

from .. import base


class LzmaCompressor(base.Compressor):

    def __init__(self, **options):
        super(LzmaCompressor, self).__init__()
        self.reset()

    def compress(self, data):  # noqa: D102
        if not data:
            return b''
        self._flushed = False
        return self._raw.compress(data)

    def finish(self):  # noqa: D102
        if not self.finished():
            self._finished = self._flushed = True
            return self._raw.flush()
        return b''

    def reset(self):  # noqa: D102
        self._raw = lzma.LZMACompressor(
            format=lzma.FORMAT_ALONE,
            check=-1,
            preset=None,
            filters=None,
        )


class LzmaDecompressor(base.Decompressor):

    def __init__(self, **options):
        super(LzmaDecompressor, self).__init__()
        self.reset()

    if sys.version_info >= (3, 5):
        def _needs_input(self):
            return self._raw.needs_input
    else:
        @staticmethod
        def _needs_input():
            return True

    def decompress(self, data):  # noqa: D102
        if not data:
            return b''
        self._flushed = False
        buffer = [self._raw.decompress(data)]
        while not self._needs_input() and not self._raw.eof:
            buffer.append(self._raw.decompress(b''))
        return b''.join(buffer)

    def reset(self):  # noqa: D102
        self._raw = lzma.LZMADecompressor(
            format=lzma.FORMAT_ALONE,
            memlimit=None,
            filters=None
        )
