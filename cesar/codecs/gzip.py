# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import zlib
import six

from .. import base


class GzipCompressor(base.Compressor):

    def __init__(self, **options):
        super(GzipCompressor, self).__init__()
        self._level = options.get('level', 1)
        self._method = options.get('method', 8)
        self._wbits = options.get('wbits', zlib.MAX_WBITS|16)
        self._memlevel = options.get('memlevel', 8)
        self._strategy = options.get('strategy', zlib.Z_DEFAULT_STRATEGY)
        self._dictionary = options.get('dictionary')
        self.reset()

    def compress(self, data):  # noqa: D102
        self._flushed = False
        return self._raw.compress(data)

    def finish(self):  # noqa: D102
        if not self.finished():
            self._finished = self._flushed = True
            return self._raw.flush()
        return b''

    if six.PY2:

        def reset(self):
            self._raw = zlib.compressobj(self._level)

    else:

        def reset(self):  # noqa: D102
            options = {'level': self._level, 'method': self._method,
                       'wbits': self._wbits, 'memLevel': self._memlevel,
                       'strategy': self._strategy}
            if self._dictionary is not None:
                options['zdict'] = self._dictionary
            self._raw = zlib.compressobj(**options)


class GzipDecompressor(base.Decompressor):

    def __init__(self, **options):
        super(GzipDecompressor, self).__init__()
        self._wbits = options.get('wbits', zlib.MAX_WBITS|32)
        self._zdict = options.get('dictionary', b'')
        self.reset()

    def decompress(self, data):  # noqa: D102
        self._flushed = False
        return self._raw.decompress(data)

    def flush(self):  # noqa: D102
        if not self.flushed():
            self._flushed = True
            return self._raw.flush()
        return b''

    def finish(self):  # noqa: D102
        if not self.finished():
            self._finished = True
            return self.flush()
        return b''

    if six.PY2:

        def reset(self):
            self._raw = zlib.decompressobj(self._wbits)

    else:

        def reset(self):  # noqa: D102
            self._raw = zlib.decompressobj(
                wbits=self._wbits,
                zdict=self._zdict
            )
