# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import bz2
import sys

import six

from .. import base


class Bz2Compressor(base.Compressor):

    def __init__(self, **options):
        super(Bz2Compressor, self).__init__()
        self._level = options.get('level', 1)
        self.reset()

    def compress(self, data):  # noqa: D102
        self._flushed = False
        return self._raw.compress(data)

    def finish(self):  # noqa: D102
        if not self.finished():
            self._finished = self._flushed = True
            return self._raw.flush()
        return b''

    def reset(self):
        self._raw = bz2.BZ2Compressor(self._level)


class Bz2Decompressor(base.Decompressor):

    def __init__(self, **options):
        super(Bz2Decompressor, self).__init__()
        self.reset()

    if sys.version_info >= (3, 5):

        def _needs_input(self):
            return self._raw.needs_input
    else:
        
        @staticmethod
        def _needs_input():
            return True

    def decompress(self, data):  # noqa: D102
        if not data:
            return b''
        self._flushed = False
        buffer = [self._raw.decompress(data)]
        while not self._needs_input() and not self._raw.eof:
            buffer.append(self._raw.decompress(b''))
        return b''.join(buffer)

    def reset(self):
        self._raw = bz2.BZ2Decompressor()
