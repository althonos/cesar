# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import io
import os
import shutil

import six
from pkg_resources import iter_entry_points

from .core import get_compressor, get_decompressor
from .base import Compressor, Decompressor


__all__ = ["CompressedFileReader", "CompressedFileWriter", "open"]


class CompressedFileWriter(io.BufferedWriter):

    def __init__(self, raw, compressor=None, buffer_size=io.DEFAULT_BUFFER_SIZE):
        super(CompressedFileWriter, self).__init__(raw, buffer_size)
        self._buffer = bytearray()
        self._buffer_size = buffer_size
        self._compressor = compressor

    def writable(self):
        return True

    def write(self, data):
        compressed_data = self._compressor.compress(data)
        self._buffer.extend(compressed_data)
        if len(self._buffer) > self._buffer_size:
            self.flush()
        return len(compressed_data)

    def flush(self):
        if self._buffer:
            self.raw.write(self._buffer)
            self._buffer = bytearray()

    def seekable(self):
        return False

    def close(self):
        if not self.closed:
            self.flush()
            self.raw.write(self._compressor.finish())
            super(CompressedFileWriter, self).close()


class CompressedFileReader(io.BufferedReader):

    def __init__(self, raw, decompressor=None, buffer_size=io.DEFAULT_BUFFER_SIZE):
        super(CompressedFileReader, self).__init__(raw, buffer_size)
        if not isinstance(decompressor, Decompressor):
            raise TypeError("<decompressor> must be a Decompressor instance")
        self._buffer_size = buffer_size
        self._buffer = bytearray()
        self._pos = 0
        self._decompressor = decompressor

    def __iter__(self):
        chunk = self.readline()
        while chunk:
            yield chunk
            chunk = self.readline()

    def read(self, size=-1):
        self._checkReadable()

        if size == 0:
            data = b''
        elif size is not None and 0 < size < len(self._buffer):
            data = self._buffer[:size]
            self._buffer = self._buffer[size:]
        else:
            data = self._buffer[:]
            while size is None or size < 0 or len(data) < size:
                chunk = self.raw.read(self._buffer_size)
                if not chunk:
                    data.extend(self._decompressor.finish())
                    break
                data.extend(self._decompressor.decompress(chunk))
            if size is not None and size > 0:
                self._buffer = data[size:]
                data = data[:size]
            else:
                self._buffer = bytearray()

        self._pos += len(data)
        return bytes(data)

    def read1(self, size):
        self._checkReadable()
        if size < 0:
            raise ValueError("read length must be positive")
        elif size == 0:
            return b''
        if not self._buffer and size:
            decompress = self._decompressor.decompress
            finish = self._decompressor.finish
            chunk = self.raw.read(min(size, self._buffer_size))
            self._buffer.extend(decompress(chunk) if chunk else finish())
        size = min(size, len(self._buffer))
        data = bytes(self._buffer[:size])
        self._buffer = self._buffer[size:]
        self._pos += len(data)
        return data

    def readline(self, size=-1):

        size = float('inf') if size is None or size < 0 else size

        while b'\n' not in self._buffer and len(self._buffer) < size:
            chunk = self.raw.read(self._buffer_size)
            if not chunk:
                self._buffer.extend(self._decompressor.finish())
                break
            self._buffer.extend(self._decompressor.decompress(chunk))

        newline = self._buffer.find(b'\n')
        if 0 <= size <= newline:
            return self.read(size)
        elif newline > -1:
            return self.read(newline+1)
        else:
            return self.read()

    def peek(self, size=-1):
        if size is None or size < 0:
            size = len(self._readbuffer)
        elif size > len(self._readbuffer):
            size = len(self._readbuffer)
        return self._readbuffer[:size]

    def readable(self):  # noqa: D102
        return True

    def rewind(self):
        if self.mode not in ('r', 'rb'):
            raise io.UnsupportedOperation('cannot rewind in write mode')
        self.raw.seek(0)
        self._pos = 0
        self._buffer = bytearray()
        self._decompressor.reset()

    def seek(self, pos, whence=os.SEEK_SET):  # noqa: D102
        self._checkSeekable()
        if whence == os.SEEK_SET:
            if pos < 0:
                raise ValueError("Negative seek position {}".format(pos))
            if pos > self._pos:
                self.seek(pos - self._pos, os.SEEK_CUR)
            else:
                self.rewind()
                self.seek(pos, os.SEEK_CUR)
        elif whence == os.SEEK_CUR:
            if pos >= 0:
                self.read(pos)
            else:
                self.seek(self._pos + pos, os.SEEK_SET)
        elif whence == os.SEEK_END:
            if pos > 0:
                raise ValueError("Positive seek position {}".format(pos))
            data = self.read()
            if len(data) >= -pos:
                self._buffer = bytearray(data[pos:] if pos else data)
                self._pos += pos
            else:
                self.seek(self._pos+pos, os.SEEK_SET)
        else:
            raise ValueError(
                "Invalid whence ({}, should be 0, 1 or 2)".format(whence))
        return self.tell()

    def seekable(self):  # noqa: D102
        return self.raw.seekable()

    def tell(self):  # noqa: D102
        return self._pos


def open(path, mode='r'):

    if mode not in ('r', 'rb', 'w', 'wb', 'x', 'xb'):
        raise ValueError('invalid mode: \'{}\''.format(mode))

    handle = io.open(path, mode='rb' if 'r' in mode else 'wb')
    ext = path.split(os.path.extsep)[-1]

    if mode in ('rb', 'r'):
        decompressor = get_decompressor(ext)
        handle = CompressedFileReader(handle, decompressor=decompressor)
    else:
        compressor = get_compressor(ext)
        handle = CompressedFileWriter(handle, compressor=compressor)

    if not 'b' in mode:
        handle = io.TextIOWrapper(handle)

    return handle
