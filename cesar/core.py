# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import pkg_resources
import six

from .base import Compressor, Decompressor


__all__  = ['get_compressor', 'get_decompressor', 'compress', 'decompress']


def get_compressor(compression, **options):
    """Get the `Compressor` implementation for a given compression.

    Expects the short name of the compression (i.e. the name used as
    the suffix of the compressed file), for instance: ``gz``, ``xz``,
    ``lzma``, ``brotli``.

    Arguments:
        compression (str): A compression name.

    Returns:
        type: a `Compressor` instance.

    Raises:
        TypeError: if ``compression`` is not a string.
        RuntimeError: if no compressor could be found for the given
        ``compression``, or if the `Compressor` could not be loaded
        successfully.

    Exemple:
        >>> import cesar
        >>> cesar.get_compressor('gz')
        <class 'cesar.compressor.gzip.GzipCompressor'>
    """
    if not isinstance(compression, six.string_types):
        raise TypeError("expected string, not '{}'".format(type(compression)))

    com = next(pkg_resources.iter_entry_points(
        'cesar.compressors', compression), None)

    if com is None:
        raise RuntimeError("could not find compressor for '{}'".format(compression))

    try:
        compressor_factory = com.load()
    except Exception as e:
        raise RuntimeError("could not load compressor for '{}'".format(compression))

    if not issubclass(compressor_factory, Compressor):
        raise TypeError("class does not implement the compressor interface")

    return compressor_factory(**options)


def get_decompressor(compression, **options):

    if not isinstance(compression, six.string_types):
        raise TypeError("expected string, not '{}'".format(type(compression)))

    dec = next(pkg_resources.iter_entry_points(
        'cesar.decompressors', compression), None)

    if dec is None:
        raise RuntimeError("could not find decompressor for '{}'".format(compression))

    try:
        decompressor_factory = dec.load()
    except Exception as e:
        raise RuntimeError("could not load decompressor for '{}'".format(compression))

    if not issubclass(decompressor_factory, Decompressor):
        raise TypeError("class does not implement the decompressor interface")

    return decompressor_factory(**options)


def compress(compression, data, **options):
    """Compress some data using the given compression.

    Arguments:
        compression (str): The name of the compression used to compress
            the data.
        data (bytes): The data to compress.

    Returns:
        bytes: the compressed data.

    Raises:
        TypeError: if ``data`` is not bytes.

    Exemple:
        >>> import cesar
        >>> cesar.compress("brotli", b"Hi!")
        b'\\x0b\\x01\\x80Hi!\\x03'

    Note:
        This function is not suitable for stream compression, i.e.
        several buffers obtained through `compress` cannot be
        concatenated together. Use the `Compressor` API to accomplish
        this.
    """
    compressor = get_compressor(compression, **options)
    return b''.join([compressor.compress(data), compressor.finish()])


def decompress(compression, data, **options):
    decompressor = get_decompressor(compression, *options)
    return b''.join([decompressor.decompress(data), decompressor.finish()])
