# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import abc
import warnings

import six


@six.add_metaclass(abc.ABCMeta)
class Compressor(object):

    def __init__(self):
        self._flushed = False
        self._finished = False

    def __del__(self):
        if self.finish():
            msg = "compressor was not finished and had buffered data"
            warnings.warn(msg, ResourceWarning)

    @abc.abstractmethod
    def compress(self, data):
        pass

    def flush(self):
        if not self.flushed():
            self._flushed = True
        return b''

    def finish(self):  # pragma: no cover
        if not self.finished():
            self._finished = True
            return self.flush()
        return b''

    def flushed(self):
        return self._flushed

    def finished(self):
        return self._finished

    def reset(self):
        pass


@six.add_metaclass(abc.ABCMeta)
class Decompressor(object):

    def __init__(self, **options):
        self._flushed = False
        self._finished = False

    def __del__(self):
        if self.finish():
            msg = "decompressor was not finished and had buffered data"
            warnings.warn(msg, ResourceWarning)

    @abc.abstractmethod
    def decompress(self, data):
        pass

    def flush(self):
        if not self.flushed():
            self._flushed = True
        return b''

    def finish(self):
        if not self.finished():
            self._finished = True
            return self.flush()
        return b''

    def flushed(self):
        return self._flushed

    def finished(self):
        return self._finished

    def reset(self):
        pass
