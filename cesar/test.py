# coding: utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import os


class CesarTestCase(object):

    _compressor = NotImplemented
    _decompressor = NotImplemented

    def setUp(self):
        self.c = self._compressor()
        self.d = self._decompressor()

    def test_flush(self):
        self.assertFalse(self.c.finished())
        self.assertFalse(self.c.flushed())

        d = self.c.flush()
        self.assertTrue(self.c.flushed())

        d += self.c.compress(os.urandom(512))
        self.assertFalse(self.c.flushed())

        d += self.c.flush()
        self.assertTrue(self.c.flushed())
        self.assertEqual(self.c.flush(), b'')

        d += self.c.finish()

        self.assertTrue(self.c.finished())
        self.assertEqual(self.c.finish(), b'')

        self.assertFalse(self.d.finished())
        self.assertFalse(self.d.flushed())

        decomp = self.d.decompress(d)
        self.assertFalse(self.d.flushed())
        decomp += self.d.flush()
        self.assertTrue(self.d.flushed())
        self.assertFalse(self.d.finished())

        decomp += self.d.finish()
        self.assertTrue(self.d.finished())
        self.assertEqual(self.d.finish(), b'')

    def test_single_part(self):
        data = os.urandom(2048)
        comp = bytearray() + self.c.compress(data) + self.c.finish()
        decomp = bytearray() + self.d.decompress(bytes(comp)) + self.d.finish()
        self.assertEqual(data, bytes(decomp))

    def test_multi_part(self):
        data = [os.urandom(512) for _ in range(4)]
        comp = []

        for d in data:
            comp.append(self.c.compress(d))
        comp.append(self.c.finish())

        decomp = bytearray()
        for c in comp:
            decomp += self.d.decompress(c)
        decomp += self.d.finish()

        self.assertEqual(b''.join(data), decomp)
